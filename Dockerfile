FROM mambaorg/micromamba:1.5.1

USER root
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential libgl1-mesa-glx \
    && rm -rf /var/lib/apt/lists/*
USER $MAMBA_USER

COPY --chown=$MAMBA_USER:$MAMBA_USER env.yaml /tmp/env.yaml
RUN micromamba install -y -n base -f /tmp/env.yaml && \
    micromamba clean --all --yes

COPY data_cleanup.py /home/data_cleanup.py

