import numpy as np
import pandas as pd
import os
import json
import sys

from typing import Optional


def main(input_dir, output_dir):
    """
    Run the phenotypic data cleanup pipeline. Processes phenotypic data by filtering,
    extracting statistics, and handling missing values.

    Args:
        input_dir (str): Directory containing 'scan_traits_csv' and 'params.json'.
        output_dir (str): Directory where the output files will be saved.
    """
    # get file paths
    scan_traits_file = os.path.join(input_dir, "scan_traits_csv")
    params_file = os.path.join(input_dir, "params.json")
    if not os.path.exists(scan_traits_file) or not os.path.exists(params_file):
        raise FileNotFoundError(
            "Required input files are not found in the input directory."
        )
    # load scan_traits_csv as a dataframe
    scan_traits_df = pd.read_csv(scan_traits_file)
    # load params.json as a dict
    with open(params_file) as f:
        params = json.load(f)

    # get the root trait start column from the params dict
    col_start = get_col_start(params)

    # make output directory if it doesn't exist
    os.makedirs(output_dir, exist_ok=True)

    # get the EDA metrics for the root traits of all samples and save them
    eda_results_all_samples = get_eda_metrics(
        scan_traits_df,
        col_start,
        os.path.join(output_dir, "eda_results_all_samples.csv"),
    )

    # find samples missing all root trait data and save them
    find_and_save_missing_all(scan_traits_df, col_start, output_dir)
    # find samples missing any root trait data and save them
    find_and_save_missing_any(scan_traits_df, col_start, output_dir)
    # find samples with some root trait data and save them--all samples with predictions
    partial_data_df = find_and_save_partial_data(scan_traits_df, col_start, output_dir)
    # find samples without any missing root trait data and save them--samples with all root trait data
    complete_data_df = find_and_save_complete_data(
        scan_traits_df, col_start, output_dir
    )

    # get the EDA metrics for the root traits of all samples with predictions and save them
    eda_results_root_predictions = get_eda_metrics(
        complete_data_df,
        col_start,
        os.path.join(output_dir, "eda_results_root_predictions.csv"),
    )


def get_col_start(params):
    """Get the root trait start column from the params dict."""
    return params.get("col_start")


def find_and_save_missing_all(df, col_start, output_dir):
    """Find rows with all root trait data missing and save them--these should be samples with no predictions."""
    root_trait_data = df.iloc[:, col_start:].to_numpy()
    missing_all = np.isnan(root_trait_data).all(axis=1)
    df_missing_all = df[missing_all]
    df_missing_all.to_csv(
        os.path.join(output_dir, "samples_missing_all_traits.csv"), index=False
    )


def find_and_save_missing_any(df, col_start, output_dir):
    """Find rows with any root trait data missing and save them."""
    root_trait_data = df.iloc[:, col_start:].to_numpy()
    missing_any = np.isnan(root_trait_data).any(axis=1)
    df_missing_any = df[missing_any]
    df_missing_any.to_csv(
        os.path.join(output_dir, "samples_missing_any_traits.csv"), index=False
    )


def find_and_save_complete_data(df, col_start, output_dir):
    """Find rows without any missing root trait data and save them."""
    root_trait_data = df.iloc[:, col_start:].to_numpy()
    no_missing_data = ~np.isnan(root_trait_data).any(axis=1)
    df_no_missing_data = df[no_missing_data]
    df_no_missing_data.to_csv(
        os.path.join(output_dir, "scan_traits_complete_data.csv"), index=False
    )
    return df_no_missing_data


def find_and_save_partial_data(df, col_start, output_dir):
    """Find rows with some root trait data and save them."""
    root_trait_data = df.iloc[:, col_start:].to_numpy()
    some_data_present = ~np.isnan(root_trait_data).all(axis=1)
    df_some_data_present = df[some_data_present]
    df_some_data_present.to_csv(
        os.path.join(output_dir, "scan_traits_partial_data.csv"), index=False
    )
    return df_some_data_present


def get_col_start(params):
    """Get the root trait start column from the params dict.

    Args:
        params (dict): Contains the root trait start column with key "col_start".

    Returns:
        The value of "col_start" in params, or None if the key does not exist.
    """
    return params.get("col_start", None)


def count_outliers_per_trait(col: pd.Series) -> pd.Series:
    """Count the number of outliers in a Pandas Series using the IQR method.

    Args:
        col (pd.Series): The pandas Series (column) for which to count the number of outliers using the IQR method.

    Returns:
        The count (scalar) of number of outliers for the input column.
    """
    Q1 = col.quantile(0.25)
    Q3 = col.quantile(0.75)
    IQR = Q3 - Q1
    lower_bound = Q1 - 1.5 * IQR
    upper_bound = Q3 + 1.5 * IQR
    outliers = col[(col < lower_bound) | (col > upper_bound)]
    return len(outliers)


def eda_computation(col: pd.Series) -> pd.Series:
    """Compute the number of NaNs, zeroes, and variance for a given pandas Series (column).

    Args:
        col (pd.Series): The pandas Series (column) for which to compute exploratory data analysis (EDA) metrics.

    Returns:
        pd.Series: A Series containing the computed EDA metrics ('Num_NaNs', 'Num_Zeroes', 'Variance').
    """
    num_nans = col.isna().sum()
    num_zeroes = (col == 0).sum()
    variance = col.var()

    return pd.Series(
        {"Num_NaNs": num_nans, "Num_Zeroes": num_zeroes, "Variance": variance}
    )


def get_eda_metrics(
    input_dataframe: pd.DataFrame,
    col_start: int,
    output_csv_path: Optional[str] = None,
    write_csv: bool = True,
) -> pd.DataFrame:
    """Perform exploratory data analysis (EDA) on a dataframe with root trait data to assess the number of NaNs,
    zeroes, outliers, and variance for each root trait.

    Args:
        input_dataframe (pd.Dataframe): Pandas dataframe object with root traits.
        col_start (int): The index of the column where root traits start.
        output_csv_path (Optional[str]): The path to save the EDA results as a CSV file. If None, results will not be saved.
        write_csv (bool): Flag indicating whether to save the EDA results to a CSV file. Default is True.

    Returns:
        pd.DataFrame: A DataFrame summarizing the EDA results.
    """

    # Use the `input_dataframe` for EDA
    df = input_dataframe.copy()

    # Select columns that represent calculated root traits (starting from `col_start`)
    trait_columns = df.columns[col_start:]

    # Apply the eda_computation function to each trait column and collect the results
    eda_results = df[trait_columns].apply(eda_computation)

    # Transpose the DataFrame for better readability
    eda_results = eda_results.T

    # Count the outliers for each trait
    outlier_counts = df[trait_columns].apply(count_outliers_per_trait)

    # Add the outlier counts to the EDA results DataFrame
    eda_results["Num_Outliers"] = outlier_counts

    # Calculate fractions based on the total number of samples
    eda_results["Fraction_NaNs"] = eda_results["Num_NaNs"] / df.shape[0]
    eda_results["Fraction_Zeroes"] = eda_results["Num_Zeroes"] / df.shape[0]
    eda_results["Fraction_Outliers"] = eda_results["Num_Outliers"] / df.shape[0]

    # Reset the index and rename the index column to 'Trait'
    eda_results.reset_index(inplace=True)
    eda_results.rename(columns={"index": "Trait"}, inplace=True)

    if write_csv and output_csv_path:
        # Save the EDA results to a CSV file
        eda_results.to_csv(output_csv_path, index=False)

    return eda_results


if __name__ == "__main__":
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    main(input_dir, output_dir)
